<?php
class Cart {
	private $cartItems = [];
	
	public function getItems() {
		return $this->cartItems;
	}
	
	//if item in cart, returns item index. otherwise returns null.
	public function getItemIndex($newItem) {
		$index = 0;
		foreach ($this->cartItems as $item) {
			if ($this->cartItems[$index]["name"] == $newItem["name"]) {
				return $index;
			}
			$index++;
		}
		return null;
	}
	
	//if item not in cart, adds item & quantity to cartItems[]
	//if item in cart, increments the quantity
	public function addItem($newItem, $quantity = 1) {
		$itemIndex = $this->getItemIndex($newItem);
		
		if ($quantity > 0) {
			if (!isset($itemIndex)) {				
				$newItem = $newItem + array('quantity' => $quantity);		
				$this->cartItems[] = $newItem; 
			}
			else {
				$this->cartItems[$itemIndex]["quantity"] += $quantity;
			}
		}
	}
	
	//removes item from cart
	//if item not in cart, no action taken
	public function removeItem($removeItem) {
		$itemIndex = $this->getItemIndex($removeItem);
		
		if (isset($itemIndex)) {
			unset($this->cartItems[$itemIndex]);
			$this->cartItems = array_values($this->cartItems); //re-index
		}
	}
	
	//reduces quantity of item in cart
	//if new quantity is < 1, removes item from cart
	//if item not in cart, no action taken
	public function reduceQuantity($item, $quantity = 1) {
		$itemIndex = $this->getItemIndex($item);
		
		if(isset($itemIndex)) { 
			$newQuantity = $this->cartItems[$itemIndex]["quantity"] - $quantity;
			
			($newQuantity > 0) ? ($this->cartItems[$itemIndex]["quantity"] -= $quantity) : $this->removeItem($item);
		}
	}
	
	//returns number of unique items in cart regardless of quantity
	public function numUniqueItems() {
		return sizeof($this->cartItems);
	}
	
	//returns subtotal price for a single item - i.e. item price * quantity
	//returns null if item not in cart
	public function itemTotalPrice($item) {
		$itemIndex = $this->getItemIndex($item);
		
		if (isset($itemIndex)) {
			return number_format($this->cartItems[$itemIndex]["price"] * $this->cartItems[$itemIndex]["quantity"],2);
		}
		
		return null;
	}

	//returns grand total price of all items in cart
	//returns 0 if cart is empty
	public function totalPrice() {
		$total = 0;
		foreach($this->cartItems as $item) {
			$total += $item["price"] * $item["quantity"];
		}
		
		return number_format($total,2);
	}
		
	//removes all items from cart
	public function emptyCart() {
		$this->cartItems = [];
	}
 }
?>