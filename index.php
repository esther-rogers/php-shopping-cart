<?php
include("database.php");
include("cart.php");

session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>Shopping Cart Demo</title>
</head>

<body>

<h2>SHOP</h2>
<!-- table of all products offered -->
<table>
	<tr>
		<th>Product name</th>
		<th>Price</th>
		<th>Quantity</th>
		<th></th>
	</tr>
	
	<?php
	$database = new Database();
	$products = $database->getProducts();
	
	$count = 0; 
	foreach ($products as $product) { 
	?>
	
	<tr>
		<td><?= $product["name"] ?></td>
		<td width=80px, style="text-align:right"><?= number_format($product["price"],2) ?></td>
		<td>
			<form method="post">
			<input type="text" name="quant<?= $count ?>" size="2"/>
			<input type="submit" name="button<?= $count ?>"
					class="button" value="Add to cart"/>
		</td>
	</tr>
	</form>
	<?php $count++; } ?>
</table>


<h2>CART</h2>

<?php
if (isset($_SESSION['cart'])) { 
	$cart = $_SESSION['cart'];
} else { 
	$cart = new Cart();
} ?>

<!-- table of all products in cart -->
<table>
	<tr>
		<th>Product name</th>
		<th>Price</th>
		<th>Quantity</th>
		<th>Item Total</th>
		<th></th>
	</tr>
<?php 
	$count = 0; 
	foreach ($cart->getItems() as $item) { 
?>
	<tr>
		<td><?= $item["name"] ?></td>
		<td width=100px, style="text-align:right"><?= number_format($item["price"],2) ?></td>
		<td width=50px, style="text-align:right"><?= $item["quantity"] ?></td>
		<td width=100px, style="text-align:right"><?= $cart->itemTotalPrice($item) ?></td>
		<td width=100px, style="text-align:right">
			<form method="post">
			<input type="submit" name="cartbutton<?= $count ?>"
					class="button" value="Remove from cart"/>
		</td>
	</tr>
	</form>
	<?php $count++; } ?>
	
	<tr>
		<td><h3>Grand Total</h3></td>
		<td></td>
		<td></td>
		<td width=100px, style="text-align:right"><h3><?= $cart->totalPrice()?></h3></td>
	</tr>
</table>	
	
<?php 
//add to cart pressed
for ($i = 0; $i < sizeof($products); $i++) { 
	if(array_key_exists('quant' . $i, $_POST) && is_numeric($_POST['quant' . $i])) {
		$cart->addItem($products[$i], $_POST['quant' . $i]);
		$_SESSION['cart'] = $cart;
		header("Refresh:0");
    }
}

//remove from cart pressed
$items = $cart->getItems();
for ($i = 0; $i < sizeof($cart->getItems()); $i++) { 
	if(array_key_exists('cartbutton' . $i, $_POST)) {
		 $cart->removeItem($items[$i], $_POST['cartbutton' . $i]);
		 $_SESSION['cart'] = $cart;
		 header("Refresh:0");
    }
}

$_SESSION['cart'] = $cart;
?>

</body>
</html>